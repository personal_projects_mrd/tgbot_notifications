import requests
from datetime import datetime


class TgBot:
    def __init__(self, token):
        self.token = token

    def check_updates(self, offset: int = 0, timeout: int = 30):
        if offset == 0:
            r = requests.get(f"https://api.telegram.org/bot{self.token}/getUpdates")
        else:
            r = requests.get(f"https://api.telegram.org/bot{self.token}/getUpdates?offset={offset}&timeout={timeout}")
        return r.json()

    def send_message(self, chat_id, content):
        return requests.get(f"https://api.telegram.org/bot{self.token}/sendMessage?chat_id={chat_id}&text={content}")


class Logwriter:
    def __init__(self) -> None:
        with open('webhook_logs.log', 'a'):
            pass

    @staticmethod
    def write(chunk: str) -> None:
        # now = datetime.now()
        # current_time = now.strftime("%H:%M:%S")
        # chunk.update({'time_local': current_time})
        with open('webhook_logs.log', 'a') as logsfile:
            logsfile.write(str(chunk))


class QueueHandler:
    def __init__(self):
        self.queue_list = list()

    def pop(self) -> dict or str and int:
        if len(self.queue_list) == 0:
            return None, None
        ind = len(self.queue_list)
        return self.queue_list.pop(), ind - 1

    def rm(self, ind) -> None:
        if len(self.queue_list) == 0:
            return None
        del self.queue_list[ind]

    def append(self, obj) -> None:
        if obj not in self.queue_list:
            self.queue_list.append(obj)

    def get_all(self) -> list:
        return self.queue_list

    def update_dict(self, num: int, obj: dict):
        self.queue_list[num].update(obj)


class WebhookHandler:
    def __init__(self):
        pass

    @staticmethod
    def pipeline(body: dict) -> str:

        try:
            output = "🚀Pipeline info:\n"\
                     f"Source: {body['object_attributes']['source']}\n"\
                     f"Triggered by: {body['user']['name']}\n"\
                     f"Project: {body['project']['name']}\n"\
                     f"Web url: {body['project']['web_url']}\n"\
                     f"Branch: {body['object_attributes']['ref']}\n"\
                     f"Commit: {body['commit']['id']}\n"\
                     f"Commit message: {body['commit']['message']}\n"\
                     f"Stages: {str(body['object_attributes']['stages'])}\n"\
                     f"Status: {body['object_attributes']['status']}\n"\
                     f"Duration: {body['object_attributes']['duration']}\n"
        except:
            output = f"Err: Can't parse body :(\n {str(body)}"
        print(output)
        return output

    @staticmethod
    def build(body: dict) -> str:
        try:
            output = "🏗Build info:\n" \
                     f"Triggered by: {body['user']['name']}\n" \
                     f"Project: {body['repository']['name']}\n" \
                     f"Web url: {body['repository']['homepage']}\n" \
                     f"Branch: {body['ref']}\n" \
                     f"Commit: {body['sha']}\n" \
                     f"Commit message: {body['commit']['message']}\n"

            if body['runner']:
                output += f"Runner: {body['runner']['description']}\n"
            output += f"Build stage: {body['build_stage']}\n"

            if body['build_status'] == 'success':
                output += f"✅Build status: {body['build_status']}✅\n"
            elif body['build_status'] == 'fail':
                output += f"🆘Build status: {body['build_status']}🆘\n"
            elif body['build_status'] == 'running':
                output += f"▶️Build status: {body['build_status']}▶️\n"
            else:
                output += f"ℹ️Build status: {body['build_status']}ℹ️\n"

            output += f"Duration: {body['build_duration']}\n"
        except:
            output = f"Err: Can't parse body :(\n {str(body)}"
        print(output)
        return output

    @staticmethod
    def push(body: dict) -> str:
        try:
            output = "🔧Push info:\n"\
                     f"Triggered by: {body['user_name']}\n"\
                     f"Project: {body['project']['name']}\n"\
                     f"Web url: {body['project']['web_url']}\n"\
                     f"Branch: {body['ref']}\n"\
                     f"Commit: {body['commits'][0]['id']}\n"\
                     f"Commit message: {body['commits'][0]['message']}\n" \
                     f"Commit URL: {body['commits'][0]['url']}\n" \
                     f"Commit author: {body['commits'][0]['author']['name']}\n" \
                     f"Added: {str(body['commits'][0]['added'])}\n"\
                     f"Modified: {str(body['commits'][0]['modified'])}\n" \
                     f"Removed: {str(body['commits'][0]['removed'])}\n"
        except:
            output = f"Err: Can't parse body :(\n {str(body)}"
        print(output)
        return output

    def handle(self, body: dict, headers: dict):
        if 'GitLab' in headers["User-Agent"]:
            if body["object_kind"] == 'pipeline':
                if body['object_attributes']['status'] == 'fail' or body['object_attributes']['status'] == 'success':
                    return self.pipeline(body)
                else:
                    return None
            elif body["object_kind"] == 'build':
                if body['build_status'] == 'fail' or body['build_status'] == 'success':
                    return self.build(body)
                else:
                    return None
            elif body["object_kind"] == 'push':
                return self.push(body)
            else:
                return body
