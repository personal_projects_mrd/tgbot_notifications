import os
import threading
import time
import json

from utils import QueueHandler, Logwriter, TgBot, WebhookHandler

import webhook_listener


token = os.environ.get('TG_TOKEN')
degub_tg_token = os.environ.get('DEBUG_TG_TOKEN')
wh_tokens = os.environ.get('WH_TOKENS').split(' ')
prefix = os.environ.get('PREFIX', '$')

bot = TgBot(token)
debug_bot = TgBot(degub_tg_token)
subscribe_notifications = list()
q_webhooks = QueueHandler()
q_updates = QueueHandler()
q_subs = QueueHandler()
whHandler = WebhookHandler()


class Listener:

    def process_post_request(self, request, *args, **kwargs):
        body_raw = request.body.read(int(request.headers['Content-Length'])) if int(
            request.headers.get('Content-Length', 0)) > 0 else '{}'
        body = json.loads(body_raw.decode('utf-8'))

        request_dict = {
            "method": request.method,
            "headers": request.headers,
            "args": args,
            "keyword args": kwargs,
            "body": body,
        }
        print('Got an request!')

        print(json.dumps(request_dict, indent=5))
        Logwriter.write(json.dumps(request_dict, indent=5) + '\n')
        self.q.append(request_dict)

        return

    def __init__(self, queue):
        self.q = queue
        webhooks = webhook_listener.Listener(handlers={"POST": self.process_post_request})
        webhooks.start()

        while True:
            time.sleep(300)


def update_checker_thread(q_updates_):
    offset = 0
    while True:
        response = bot.check_updates(offset=offset)
        if response["ok"] is False:
            Logwriter.write("ERROR TRYING TO CONNECT TO THE SERVER")
            continue
        for result in response["result"]:
            if result["update_id"] >= offset:
                offset = result["update_id"] + 1
            q_updates_.append(result)


def updates_handler_thread(q_updates_, q_subs_):
    while True:
        current_item, index = q_updates_.pop()
        if current_item is None:
            continue
        if 'message' in current_item:
            if 'text' in current_item["message"]:
                if current_item["message"]["text"].startswith(prefix):
                    cmd_handler(current_item["message"]["text"], current_item["message"]["chat"]["id"], q_subs_)


def cmd_handler(message: str, chat_id: int, q_subs_: QueueHandler) -> None:
    args = message.split(" ")
    args[0] = args[0][1:]
    if args[0] == "sub":
        bot.send_message(chat_id=chat_id, content="This chat is now subscribed to the webhooks notifications!")
        q_subs_.append(chat_id)
    if args[0] == "unsub":
        bot.send_message(chat_id=chat_id, content="This chat is now unsubscribed from the webhooks notifications!")
        subs = q_subs_.get_all()
        if chat_id in subs:
            ind = subs.index(chat_id)
        q_subs_.rm(ind=ind)
    if args[0] == "repo":
        bot.send_message(chat_id=chat_id, content="https://gitlab.com/personal_projects_mrd/tgbot_notifications")

def webhook_listener_thread(q_webhooks_):
    webhook_listener_ = Listener(q_webhooks_)


def webhook_check_for_updates_thread(q_webhooks_, q_subs_):
    while True:
        log, _ = q_webhooks_.pop()
        if log is not None:
            for chat_id in q_subs_.get_all():
                if log['headers']['X-Gitlab-Token'] in wh_tokens:
                    content_raw = f"headers: {log['headers']}\n" \
                                + f"body: {log['body']}"
                    content = whHandler.handle(body=log['body'], headers=log['headers'])
                    if content is not None:
                        bot.send_message(chat_id=chat_id, content=content)
                else:
                    for part in log:
                        debug_bot.send_message(chat_id=123121982, content=str(log[part]))


wlt = threading.Thread(target=webhook_listener_thread, args=[q_webhooks, ])
wlt.start()

wcfut = threading.Thread(target=webhook_check_for_updates_thread, args=[q_webhooks, q_subs])
wcfut.start()

uct = threading.Thread(target=update_checker_thread, args=[q_updates, ])
uct.start()

uht = threading.Thread(target=updates_handler_thread, args=[q_updates, q_subs])
uht.start()
